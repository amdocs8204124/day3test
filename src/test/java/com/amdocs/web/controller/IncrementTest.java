package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {
    @Test
    public void decreasecounter() throws Exception {

        int k= new Increment().decreasecounter(0);
        assertEquals("decreasecounter",0, k);        
    }
 @Test
    public void decreasecounter1() throws Exception {

        int k= new Increment().decreasecounter(1);
        assertEquals("decreasecounter",1, k);        
    }
	@Test
    public void decreasecounter2() throws Exception {

        int k= new Increment().decreasecounter(2);
        assertEquals("decreasecounter",2, k);        
    }
}
